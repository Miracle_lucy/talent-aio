<h1>talent-aio项目全面升级到<a href="http://gitee.com/tywo45/t-io" target="_blank">t-io项目</a>，非常感谢大家的支持！</h1>

<h2>花一分钟从talent-aio升到t-io，步骤如下（全是搜索替换操作）</h2>
<ol>
	<li><h3>com.talent.aio替换成org.tio</h3></li>
	<li><h3>com.talent-aio替换成org.t-io</h3></li>
	<li><h3>talent-aio-server替换成tio-core</h3></li>
	<li><h3>talent-aio-common替换成tio-core</h3></li>
	<li><h3>talent-aio-client替换成tio-core</h3></li>
	<li><h3>把你的talent-aio版本替换成t-io的最新版本，譬如把1.6.7.v20170328-RELEASE替换成1.6.8.v20170329-RELEASE</h3></li>
</ol>
